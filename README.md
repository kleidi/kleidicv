<!--
SPDX-FileCopyrightText: 2023 - 2024 Arm Limited and/or its affiliates <open-source-office@arm.com>

SPDX-License-Identifier: Apache-2.0
-->

# KleidiCV

The KleidiCV library provides high-performance image processing functions for AArch64.
It is designed to be simple to integrate into a wide variety of projects.

## Documentation

* [Platform Support](doc/platform-support.md)
* [Building](doc/build.md)
* [OpenCV Hardware Adapter Layer](doc/opencv.md)
* [Overview of KleidiCV functionality](doc/functionality.md)
* [C API documentation](https://kleidi.sites.arm.com/kleidicv/)
* [Benchmarking](doc/benchmark.md)
* [Testing](doc/test.md)
