// SPDX-FileCopyrightText: 2023 - 2024 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: Apache-2.0

#include "kleidicv/kleidicv.h"
#include "kleidicv/sve2.h"

namespace kleidicv::sve2 {

template <typename ScalarType>
class SaturatingMultiply final : public UnrollTwice {
 public:
  using ContextType = Context;
  using VecTraits = KLEIDICV_TARGET_NAMESPACE::VecTraits<ScalarType>;
  using VectorType = typename VecTraits::VectorType;

  explicit SaturatingMultiply(double scale = 1.0) : scale_{scale} {};

  VectorType vector_path(ContextType ctx, VectorType src_a, VectorType src_b) {
    VectorType result;
    (void)ctx;

    // multiply-widen even-indexed elements
    auto bottom_part = svmullb(src_a, src_b);
    // multiply-widen odd-indexed elements
    auto top_part = svmullt(src_a, src_b);
    // saturating-narrow even-indexed
    auto narrow_bottom = svqxtnb(bottom_part);
    // saturaning-narrow odd-indexed and merge with even-indexed
    result = svqxtnt(narrow_bottom, top_part);

    /* TODO: figure out the way to multiply by double or some
      fixed supported scale.
      */

    return result;
  }

 private:
  double scale_;
};

template <typename T>
kleidicv_error_t saturating_multiply(const T *src_a, size_t src_a_stride,
                                     const T *src_b, size_t src_b_stride,
                                     T *dst, size_t dst_stride, size_t width,
                                     size_t height, double scale) {
  CHECK_POINTER_AND_STRIDE(src_a, src_a_stride, height);
  CHECK_POINTER_AND_STRIDE(src_b, src_b_stride, height);
  CHECK_POINTER_AND_STRIDE(dst, dst_stride, height);
  CHECK_IMAGE_SIZE(width, height);

  (void)scale;  // TODO: figure out the way to process the scale.
  SaturatingMultiply<T> operation;
  Rectangle rect{width, height};
  Rows<const T> src_a_rows{src_a, src_a_stride};
  Rows<const T> src_b_rows{src_b, src_b_stride};
  Rows<T> dst_rows{dst, dst_stride};
  apply_operation_by_rows(operation, rect, src_a_rows, src_b_rows, dst_rows);
  return KLEIDICV_OK;
}

#define KLEIDICV_INSTANTIATE_TEMPLATE(type)                                    \
  template KLEIDICV_TARGET_FN_ATTRS kleidicv_error_t                           \
  saturating_multiply<type>(const type *src_a, size_t src_a_stride,            \
                            const type *src_b, size_t src_b_stride, type *dst, \
                            size_t dst_stride, size_t width, size_t height,    \
                            double scale)

KLEIDICV_INSTANTIATE_TEMPLATE(uint8_t);
KLEIDICV_INSTANTIATE_TEMPLATE(int8_t);
KLEIDICV_INSTANTIATE_TEMPLATE(uint16_t);
KLEIDICV_INSTANTIATE_TEMPLATE(int16_t);
KLEIDICV_INSTANTIATE_TEMPLATE(int32_t);

}  // namespace kleidicv::sve2
